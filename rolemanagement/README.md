Installing node modules
> npm install express
> npm install acl
> npm install body-parser
> npm install mongoose


How to run the server?
> npm install

> node server/server.js


Access the application
You application will run on port: 3000
API:
GET:    http://localhost:3000/users
GET:    http://localhost:3000/users/:userId
POST:    http://localhost:3000/users
PUT:     http://localhost:3000/users/:userId
DELETE:  http://localhost:3000/users/:userId