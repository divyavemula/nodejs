var mongodb = require( 'mongodb' ),
    express = require( 'express' ),
    node_acl = require( 'acl' ),
    port = 3500,
    app = express(),
    // The actual acl will reside here
    acl;
app.use( app.router );
app.use( function( error, request, response, next ) {
    if( !error ) return next();
    response.send( error.msg, error.errorCode );
});

// Connecting to our mongo database
mongodb.connect( 'mongodb://127.0.0.1:27017/acl_example', _mongo_connected );

function _mongo_connected( error, db ) {

    var mongoBackend = new node_acl.mongodbBackend( db  );

    // Create a new access control list by providing the mongo backend
    //  Also inject a simple logger to provide meaningful output
    acl = new node_acl( mongoBackend, logger() );

    // Defining roles and routes
    set_roles();
    set_routes();
}
