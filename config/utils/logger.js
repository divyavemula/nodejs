const { createLogger, transports, format } = require('winston');

const logger = createLogger({
    transports: [
        new transports.File({
            filename: 'app-info.log',
            level: 'info',
            format: format.combine(format.timestamp(), format.json())
        })
    ],
    exceptionHandlers: [
        new transports.File({ filename: 'app-exceptions.log' })
      ]
})

module.exports = logger;