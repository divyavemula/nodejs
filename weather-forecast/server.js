const express = require('express');
const bodyParser = require('body-parser');
const request = require('request');
const app = express()


//
const apiKey = '';
//Database connection --
var mongoose = require('mongoose')
mongoose.Promise = bluebird;
let url = "url+city+"&"+appId;"
mongoose.connect(url)
  .then(() => {
    console.log("Succesfully Connected to theMongodb Database..")
  })
  .catch(() => {
    console.log("Error Connecting to the Mongodb Database...")
  })
app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: true }));
app.set('view engine', 'ejs')

app.get('/', function(req, res, next) {
  res.render('index', {'body':'', forecast: ''});
 });

app.post('/weather', function(req, res, next){
  let city = req.body.city;
  if(apiKey == '' || apiKey == undefined)
  url = url+city+"&"+appId;

 request(url, function (error, response, body) {
      body = JSON.parse(body);
      if(error && response.statusCode != 200){
        throw error;
      }
    let country = (body.sys.country) ? body.sys.country : '' ;
    let forecast = "For city "+city+', country '+country;
    res.render('index', {body : body, forecast: forecast});
   });
});

app.listen(3000, function () {
  console.log('Listening on port 3000')
})
