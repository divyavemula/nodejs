# Weather app

## Goal: To get weather details for a city

## Solution: Which weather API to use?
```
https://openweathermap.org

```

## UI to be provided:
```
1. Enter a city name
2. On hit, get weather details & display on the page

```

## Todo:

```
Store the weather details in MongodDB along with CityName.
Assume your own schema absed on the response.

```