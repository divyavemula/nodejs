/* Build a node js server with below http methods:

- GET, 
i/p: /employee
o/p: respond with { "message":"Welcome", "employee": {"firstName":"Raju", "lastName":"R", "id":"678978"} }

- POST,
route: /employee 
i/p: {"firstName":"Raju", "lastName":"R", "id":"678978"}
o/p: respond with {"message":"Welcome Raju"} */
const express = require('express');
const bodyParser = require('body-parser');
var server =  express();

server.use(bodyParser.json());
server.use(bodyParser.urlencoded({extended: false}));

server.get('/employee', function(request, response) {
    response.send({"message":"Welcome", 
    "employee": {"firstName":"Raju", "lastName":"R", "id":"678978"} });
})

server.post('/employee', (request, response) => {
    console.log(request.body);
    response.send({"message":`Welcome ${request.body.firstName}`});
})

server.listen(3000, function(){
            console.log("Server started at port 3000")
        })
