/* Implement  a node js service to download below files:
1. from a url: https://file-examples-com.github.io/uploads/2017/10/file_example_JPG_100kB.jpg
2.a file from your computer (any path, but donot hard code)
 */


const {DownloaderHelper } = require ("node-downloader-helper")
const dl = new DownloaderHelper("https://file-examples-com.github.io/uploads/2017/10/file_example_JPG_100kB.jpg",__dirname)
dl.on("end",() => console.log("Download completed"))
dl.start()