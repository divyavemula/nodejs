const express = require("express");
const app = express();
const Routes = require("./server/router/upload");

app.use(express.urlencoded({ extended: true }));
Routes(app);

let port = 3000;
app.listen(port, () => {
  console.log(`Running at localhost:${port}`);
});
