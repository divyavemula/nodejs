const express = require('express');
const mongoose = require('mongoose');
const gridfs = require('gridfs-stream');
var fs = require('fs');
 
var app = express();
 
/*
  Make a MongoDB connection
*/
mongoose.connect('mongodb://localhost:27017/test')
mongoose.Promise = global.Promise;
 
gridfs.mongo = mongoose.mongo;
/*
  Check MongoDB connection
*/
var connection = mongoose.connection;
connection.on('error', console.error.bind(console, 'connection error:'));
 
connection.once('open', () => {
 
    var gfs = gridfs(connection.db);
 



  // Delete a file from MongoDB
  app.get('/api/file/delete', (req, res) => {
  
  var filename = req.query.filename;
  
  gfs.exist({ filename: filename }, (err, file) => {
    if (err || !file) {
      res.status(404).send('File Not Found');
      return;
    }
    
    gfs.remove({ filename: filename }, (err) => {
      if (err) res.status(500).send(err);
      res.send('File Deleted');
    });
  });
  });

  var server = app.listen(3000, () => {
  
  var host = server.address().address
  var port = server.address().port
 
  console.log("App listening at http://%s:%s", host, port); 
});

});

