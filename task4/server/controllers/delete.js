const deleteFile = require("../middleware/delete");

const deleteFile = async (req, res) => {
  try {
    await delete(req, res);

    console.log(req.file);
    if (req.file == undefined) {
      return res.send("select a file");
    }

    return res.send("delete the file");
  } catch (error) {
    console.log(error);
    return res.send("Error when trying delete : ${error}");
  }
};

module.exports = {
  deleteFile: deleteFile
};
