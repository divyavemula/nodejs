
const express   = require('express')
const server    = express()
const uuid = require("uuid");
const logger    = require('./utils/logger')

/* Set the logger for entire application to log the request body */
server.use(function(request, response, next){
    logger.info("Request Body ", request.body)
    logger.info("Request Query ", request.query,uuid.v4())
    logger.info("Request Headers ", request.headers,uuid.v4())
    let responseData = response.send;
    response.send   = function(data){
        logger.info(JSON.parse(data))
        responseData.apply(response, arguments)
    }    
    
})

const employee = require('./routes/employee')
server.use('/employee', employee)

server.listen(3000, function(){
    logger.info("Server is listening at port 3000");
})