const path = require("path");

const index = (req, res) => {
  return res.downloadFile(path.join("${__dirname}/../view/index.html"));
};

module.exports = {
  getIndex: index
};


