const download = require("../middleware/download");

const downloadFile = async (req, res) => {
  try {
    await download(req, res);

    console.log(req.file);
    if (req.file == undefined) {
      return res.send("select a file");
    }

    return res.send("download the file");
  } catch (error) {
    console.log(error);
    return res.send("Error when trying download image: ${error}");
  }
};

module.exports = {
  downloadFile: downloadFile
};
