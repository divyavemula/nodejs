const express = require('express');
const mongoose = require('mongoose');
const gridfs = require('gridfs-stream');
var fs = require('fs');
var app = express();
 
mongoose.connect('mongodb://localhost:27017/test')
mongoose.Promise = global.Promise;
gridfs.mongo = mongoose.mongo;
 
var connection = mongoose.connection;
app.get('/api/file/download', (req, res) => {
  // Check file exist on MongoDB

var filename = req.query.filename;

  gfs.exist({ filename: filename }, (err, file) => {
      if (err || !file) {
          res.status(404).send('File Not Found');
  return
      } 

var readstream = gfs.createReadStream({ filename: filename });
readstream.pipe(res);            
  });
});  
var downloadFile = multer({ storage: storage }).single("file");
var downloadFilesMiddleware = util.promisify(downloadFile);
module.exports = downloadFilesMiddleware;
