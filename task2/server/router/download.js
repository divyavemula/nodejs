const express = require("express");
const router = express.Router();
const indexController = require("../controllers/index");
const downloadController = require("../controllers/download");
let routes = app => {
  router.get("/", indexController.getIndex);

  router.get("/download", downloadController.downloadFile);

  return app.use("/", router);
};

module.exports = routes;





