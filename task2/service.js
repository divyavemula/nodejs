const express = require('express');
const mongoose = require('mongoose');
var gridfs = require('gridfs-stream');
var fs = require('fs');
 
var app = express();
 
/*
  Make a MongoDB connection
*/
mongoose.connect('mongodb://localhost:27017/test')
mongoose.Promise = global.Promise;
 
gridfs.mongo = mongoose.mongo;
/*
  Check MongoDB connection
*/
var connection = mongoose.connection;
connection.on('error', console.error.bind(console, 'connection error:'));
 
connection.once('open', () => {
 
    var gfs = gridfs(connection.db);
 
    app.get('/', (req, res) => {
        res.send('Download/Upload GridFS files to MongoDB <br>- by grokonez.com');
    });
 
    
 
    // Download a file from MongoDB - then save to local file-system
    app.get('/api/file/download', (req, res) => {
        // Check file exist on MongoDB
    
    var filename = req.query.filename;
    
        gfs.exist({ filename: filename }, (err, file) => {
            if (err || !file) {
                res.status(404).send('File Not Found');
        return
            } 
      
      var readstream = gfs.createReadStream({ filename: filename });
      readstream.pipe(res);            
        });
    });
 
   
    var server = app.listen(3000, () => {
    
    var host = server.address().address
    var port = server.address().port
   
    console.log("App listening at http://%s:%s", host, port); 
  });
 
});